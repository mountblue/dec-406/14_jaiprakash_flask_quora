from flask import Flask
from .config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from celery import Celery
from elasticsearch import Elasticsearch
import certifi

quora_app = Flask(__name__)
# with quora_app.app_context():
#     init_db()
quora_app.config.from_object(Config)
login = LoginManager(quora_app)
login.login_view = 'login'
db = SQLAlchemy(quora_app)
migrate = Migrate(quora_app,  db)

celery = Celery(quora_app, broker=quora_app.config['CELERY_BROKER_URL'], backend='rpc://')
# celery.conf.update(quora_app.config)
quora_app.elasticsearch = Elasticsearch(quora_app.config['ELASTICSEARCH_URL'],use_ssl=True, ca_certs=certifi.where())

from quora import routes, models, errors
