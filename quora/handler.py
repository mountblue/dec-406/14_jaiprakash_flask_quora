from flask import request
from flask import render_template, flash, redirect, url_for, request, send_file
from quora import db, celery
from .models import User, Post, Answer
from quora.search import add_to_index, remove_from_index, query_index
import csv


def get_searched_data(search_pattern):
    print("args=", search_pattern)
    data = []
    questions = query_index('posts', search_pattern, 1, 100)
    for question in questions[1]:
        temp = {}
        db_question = Post.query.filter_by(question=question['question']).first()
        # print("jjjj===",question,db_question)
        if db_question is not None:
            temp['author'] = User.query.filter_by(id=db_question.user_id).first()
            temp['question'] = question['question']
            temp['id'] = db_question.id
            temp['date'] = db_question.question_time
            answers = Answer.query.filter_by(question_id=db_question.id).all()
            temp['answer'] = []
           
            for answer in answers:
                temp_answer = {}
                temp_answer['ans'] = answer.answer
                temp_answer['author'] = User.query.filter_by(id=answer.user_id).first().username
                temp['answer'].append(temp_answer)
            data.append(temp)
    return data


def get_data():
    data = []
    questions = Post.query.all()
    for question in questions:
        add_to_index('posts', question)
        temp = {}
        temp['author'] = User.query.filter_by(id=question.user_id).first()
        temp['id'] = question.id
        temp['question'] = question.question
        temp['date'] = question.question_time
        answers = Answer.query.filter_by(question_id=question.id).all()
        temp['answer'] = []
        for answer in answers:
            temp_answer = {}
            temp_answer['ans'] = answer.answer
            temp_answer['author'] = User.query.filter_by(id=answer.user_id).first().username
            temp['answer'].append(temp_answer)
        data.append(temp)
    return data


@celery.task()
def create_user_data_csv(user_data, username):
    filename = 'quora/user_data/'+username+'_data.csv'
    print("file_name="+filename)
    with open(filename, 'w') as csvfile:
        filewriter = csv.writer(csvfile, delimiter=',')
        filewriter.writerow(['Question', 'Answer'])
        for data in user_data:
            filewriter.writerow(data)