from quora import db, login
from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    posts = db.relationship('Post',cascade = "all,delete", backref='author', lazy='dynamic')
    answers = db.relationship('Answer',cascade = "all,delete", backref='author', lazy='dynamic')

    def __init__(self, username, email): 
        self.username = username
        self.email = email

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        # password_hash = self.set_password(password)
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return self.username


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(140))
    question_time = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    answers = db.relationship('Answer',cascade = "all,delete", backref='question', lazy='dynamic')

    __searchable__ = ['question']

    def __repr__(self):
        return self.question


class Answer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    answer = db.Column(db.String(140))
    answer_time = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    question_id = db.Column(db.Integer, db.ForeignKey('post.id'))
   
    def __repr__(self):
        return str(self.answer)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


