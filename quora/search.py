from quora import quora_app


def add_to_index(index, model):
    if not quora_app.elasticsearch:
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    quora_app.elasticsearch.index(index=index, doc_type=index, id=model.id,
                                  body=payload)


def remove_from_index(index, model):
    if not quora_app.elasticsearch:
        return
    quora_app.elasticsearch.delete(index=index, doc_type=index, id=model.id)


def query_index(index, query, page, per_page):
    if not quora_app.elasticsearch:
        return [], 0
    search = quora_app.elasticsearch.search(
        index=index, doc_type=index,
        body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
              'from': (page - 1) * per_page, 'size': per_page})
    ids = [hit['_id'] for hit in search['hits']['hits']]
    question = search['hits']['hits']
    temp = []
    for each in question:
        temp.append(each['_source'])

    return ids, temp