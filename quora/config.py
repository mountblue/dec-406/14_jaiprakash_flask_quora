import os
import quora


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    FLASK_APP = quora
    FLASK_DEBUG = 1
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
    #     'sqlite:///' + os.path.join(basedir, 'quora_app.db')
    SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/quora_db'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    CELERY_BROKER_URL = 'amqp://guest@localhost//'
    CELERY_RESULT_BACKEND = 'amqp//guest@localhost//'
    
    CELERY_ACCEPT_CONTENT = ['json']
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_RESULT_SERIALIZER = 'json'
    CELERYD_PREFETCH_MULTIPLIER = 1
    CELERY_EVENT_SERIALIZER = 'json' 
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    