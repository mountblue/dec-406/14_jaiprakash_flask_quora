from quora import quora_app, db, celery
from quora.search import add_to_index, remove_from_index, query_index
from flask import render_template, flash, redirect, url_for, request, send_file, jsonify
from werkzeug.urls import url_parse
from .forms import LoginForm, RegistrationForm, AnswerForm, UpdateAccountForm
from flask_login import current_user, login_user, logout_user, login_required
from .models import User, Post, Answer
from .handler import get_data, create_user_data_csv, get_searched_data
from elasticsearch import Elasticsearch
from quora import handler
import json
import csv,time

@quora_app.route('/delete/posts/<int:post_id>/', methods=['GET', 'POST'])
def delete_question(post_id):
    if current_user.is_anonymous:
        flash('You Dont Have Priviliges .You Must Login ', 'error')
    elif current_user.is_authenticated:
        question = Post.query.filter_by(id=post_id).first()
        db.session.delete(question)
        remove_from_index('posts', question)
        db.session.commit()
    return redirect('home')


@quora_app.route('/post/<int:post_id>/answers', methods=['GET', 'POST'])
def process_question(post_id):
    if current_user.is_anonymous:
        flash('You Dont Have Priviliges .You Must Login ', 'error')

    elif current_user.is_authenticated:
        question = Post.query.filter_by(id=post_id).first()
        answer_form = AnswerForm()
        return render_template('answer.html', ans_form=answer_form, question_details=question,post_id=post_id)
    
    return redirect('home')

@quora_app.route('/submit/<int:post_id>/answers', methods=['GET', 'POST'])
def post_answer(post_id):
    answer_form = request.form
    ans = Answer(answer=answer_form['answer'],
                 user_id=current_user.get_id(), question_id=post_id)
    db.session.add(ans)
    db.session.commit()
    # print(ans)
    # print("-----------------------------------")
    # print("answer----form", answer_form)
    flash("Answer Posted")
    return redirect(url_for('home'))


@quora_app.route('/ask_question', methods=['GET', 'POST'])
def ask_question():
    if current_user.is_authenticated:
        ques = request.form
        if ques['quest']:
            # print("current user ===", current_user)
            question = Post(question=ques['quest'], user_id=User.query.filter_by(
                username=str(current_user)).first().id)
            add_to_index('posts', question)
            db.session.add(question)
            db.session.commit()
            flash("Question Posted")

        else:
            flash("Question Not Posted", "error")
        # print("ques=", ques['quest'])
        return redirect(url_for('home'))
    else:
        flash("You Have To Login To Ask Question", "error")
        return redirect(url_for('login'))


@quora_app.route('/')
# @quora_app.route('/home')
def home():
    print("url=", request.args.get('search'))
    serach_pattern = request.args.get('search')
    print("cond= ", serach_pattern is not None, serach_pattern != "")
    if serach_pattern is not None and serach_pattern != "":
        posts = get_searched_data(serach_pattern)
    else:
        posts = get_data()
    # print(posts)
    return render_template('home.html', posts=posts)


@quora_app.route('/about')
@login_required
def about():
    return render_template('about.html', posts=posts)


@quora_app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    # print("form=", form.username.data, form.password.data)
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        # print("---", type(user))
        # print("passwd = ")
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password', 'error')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        # print("next_page = ", next_page)
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('home')
        return redirect(next_page)

    return render_template('login.html', form=form)


@quora_app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('home'))


@quora_app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        flash("Logout First Than Register", 'warning')
        return redirect('home')
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(form.username.data, form.email.data)
        user.set_password(form.password.data)
        # print(user)
        db.session.add(user)
        db.session.commit()
        flash("Registration Successfull")
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form,)


@celery.task()
@quora_app.route('/download')
def download():
    path = 'quora/user_data/'+str(current_user)+'_data.csv'
    user_id = current_user.get_id()
    questions_asked = Post.query.filter_by(user_id=user_id).all()
    questions_answered = Answer.query.filter_by(user_id=user_id).all()
    user_data = []
    for questions in questions_asked:
        temp = {
            'id': questions.id,
            'question': questions.question,
            'question_time': questions.question_time,
            'user_id': questions.user_id
        }
        user_data.append([temp['question'], None])
    for answer in questions_answered:
        question = str(Post.query.filter_by(id=answer.question_id).first())

        temp = {
            'id': answer.id,
            'answer': answer.answer,
            'question_id': answer.question_id,
            'user_id': questions.user_id
        }
        file1 = user_data.append([question, temp['answer']])
        print("file1=", file1)

    print(user_data)
    task_id = create_user_data_csv.delay(user_data, str(current_user))
    # create_user_data_csv.delay(user_data, str(current_user))
    # path = 'HELLO',
    return render_template('download.html', id=task_id)
    

@quora_app.route('/profile', methods=['GET', 'POST'])
def profile():
    form = UpdateAccountForm()
    if form.validate_on_submit():   #if from is valid
        current_user.username = form.username.data      # update username
        current_user.email = form.email.data        # update email
        db.session.commit()
        flash('Your account has been updated!', 'success')
        return redirect(url_for('profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
    # image_file = url_for('static', filename='profile_pics/' + current_user.image_file)# user profile-pic, from static folder
    #    return render_template('account.html', title='Account',
    #                           image_file=image_file, form=form)
    return render_template('profile.html', title='Account', form=form)


@quora_app.route('/get_status', methods=['GET'])
def get_status():
    task_id = request.args.get('id')
    print("task_id = ", task_id)
    
    result = handler.create_user_data_csv.AsyncResult(task_id)
    time.sleep(5)
    print("result=",result.ready() )

    return jsonify({'status': result.ready()})

@quora_app.route('/downloadfile', methods=['GET', 'POST'])
def download_file():
    path = 'user_data/'+str(current_user)+'_data.csv'
    return send_file(path, as_attachment=True)